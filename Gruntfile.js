module.exports = function(grunt) {
	//配置参数
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		concat: {
			options: {
				separator: ';',
				stripBanners: true
			},
			dist: {
				src: [
					"src/JDialog.js"
				],
				dest: "dist/JDialog.js"
			}
		},
		uglify: {
			options: {
			},
			dist: {
				files: {
					'dist/JDialog.min.js': 'dist/JDialog.js'
				}
			}
		},

		copy: {
			main: {
				files: [
					{expand: true, cwd: 'src/', src: ['css/**'], dest: 'dist/'}
				],
			},
		},

		cssmin: {
			options: {
				keepSpecialComments: 0
			},
			compress: {
				files: {
					'dist/css/JDialog.css': [
						"src/css/JDialog.css"
					]
				}
			}
		}
	});

	//载入concat和uglify插件，分别对于合并和压缩
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-copy');

	//注册任务
	grunt.registerTask('default', ['concat', 'uglify', 'copy', 'cssmin']);
}